<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function news()
	{
		return $this->belongsToMany(News::class);
	}

	public function getRouteKeyName()
	{
		// By default it is the id, but we want to perform condition on the 'name' of the tag
		return 'name';
	}
}
