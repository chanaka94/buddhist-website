<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{
    public function index(News $news)
    {
        $news = $news->all()->where('published', '=', 1);
        return view('front.news.index', compact('news'));
    }

    public function view($id)
    {
        $news = News::find($id);
        return view('front.news.view', compact('news'));
    }
}
