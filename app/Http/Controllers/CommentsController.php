<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(News $news)
    {
    	$this->validate(request(), ['body' => 'required|min:2']);
    	$news->addComment(request('body'));
		return back();
    }
}
