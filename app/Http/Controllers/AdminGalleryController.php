<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class AdminGalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Gallery $gallery)
    {
        $allGallery = $gallery->all();

        // $posts = (new \App\Repositories\Posts)->all();

        // Letting the query scope to handle the filter
        // $posts = Post::latest()
        //     ->filter(request(['month', 'year']))
        //     ->get();

    	return view('admin.manage-gallery.index', compact('allGallery'));
    	// return view('admin.manage-news.index', compact('news'));
    }
}
