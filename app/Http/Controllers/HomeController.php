<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Image;
use App\Video;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $latestNews = News::orderBy('created_at', 'desc')->limit(2)->get();
        $latestImages = Image::orderBy('created_at', 'desc')->limit(2)->get();
        $latestVideos = Video::orderBy('created_at', 'desc')->limit(1)->get();
        // dd($latestVideo->url);
        return view('front.index', compact('latestNews', 'latestImages', 'latestVideos'));
    }

    public function donation()
    {
        return view('front.donation');
    }

    public function aboutUs()
    {
        return view('front.about-us');
    }

    // public function contactUs()
    // {
    //     return view('front.contact-us');
    // }

    public function visitPlan()
    {
        return view('front.plan-visit');
    }

    public function eventsCalendar()
    {
        return view('front.events-calendar');
    }
}
