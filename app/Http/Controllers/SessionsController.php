<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function __construct()
	{
		$this->middleware('guest', ['except' => 'destroy']);
	}

    public function create()
    {
    	return view('admin.sessions.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'email' => 'bail|required|email',
            'password' => 'bail|required|max:15|min:6',
        ]);
        
    	// Attempt to authenticate the user
    	if (! auth()->attempt(request(['email', 'password']))) {
            // If you have validation errors you can manualy redirect back
    		return back()->withErrors([
                'message' => 'Please check your credentials and try again'
            ]);
    	}

        // Redirect to the dashboard
        return redirect('/admin/dashboard');
    }

    public function destroy()
    {
    	auth()->logout();
    	return redirect('/login');
    }
}
