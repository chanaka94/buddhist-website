<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Video;
use DB;

class AdminVideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Video $video)
    {
        $allVideos = $video->all();

        // $posts = (new \App\Repositories\Posts)->all();

        // Letting the query scope to handle the filter
        // $posts = Post::latest()
        //     ->filter(request(['month', 'year']))
        //     ->get();

    	return view('admin.manage-videos.index', compact('allVideos'));
    	// return view('admin.manage-images.index');
    }

    public function show($video)
    {
        $video = Video::find($video);
    	return view('admin.manage-videos.show', compact('video'));
    }

    public function create()
    {
    	return view('admin.manage-videos.create');
    }

    public function store(Request $request)
    {
        // $this->validate(request(), [
        //     'title' => 'bail|required|max:100|min:3',
        //     'body' => 'bail|required|max:5000|min:10',
        //     'image' => 'bail|required',
        //     'url' => 'nullable|max:1000|min:5',
        //     'status' => 'bail|required|in:published,unpublished'
        // ]);

        DB::beginTransaction();
        try {
            $video = Video::create([
                'title' => request('title'),
                'url' => request('video'),
                'published' => request('status')
            ]);
            DB::commit();
            session()->flash('success', 'Your video URL has now been added');
            // And then redirect to the dashboard
            return redirect('/admin/dashboard/videos');
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback();
            session()->flash('failed', 'Something went wrong when adding the video URL. Please try again.');
            return redirect('/admin/dashboard/videos');
        }
    }

    public function edit($video)
    {
        $video = Video::find($video);
    	return view('admin.manage-videos.update', compact('video'));
    }

    public function update(Request $request, $id)
    {
        // $this->validate(request(), [
        //     'title' => 'bail|required|max:100|min:3',
        //     'body' => 'bail|required|max:5000|min:10',
        //     'image' => 'bail|required',
        //     'url' => 'nullable|max:1000|min:5',
        //     'status' => 'bail|required|in:published,unpublished'
        // ]);

        // Deleting the old image
        // $deletingFile = News::find($id);
        // Storage::delete('/public/storage/' . $deletingFile->image);
        // $imagePath = Storage::disk('public')->put("news_images", request('image'));

        DB::beginTransaction();
        try {
            DB::table('videos')->where('id', '=', $id)->update([
                'title' => request('title'),
                'url' => request('video'),
                'published' => request('status')
            ]);
            DB::commit();
            session()->flash('success', 'Your video URL has now been successfully updated.');
            return redirect('/admin/dashboard/videos');
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback();
            session()->flash('failed', 'Something went wrong when updating the video URL. Please try again.');
            return redirect('/admin/dashboard/videos');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('videos')->where('id', '=', $id)->delete();

            // Image deletion to be handled
            // $image = Image::find($id);
            // \File::delete( public_path('/' . $image->path));
            
            DB::commit();
            session()->flash('success', 'Successfully deleted the video URL');
            return redirect('/admin/dashboard/videos');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('failed', 'Something went wrong when deleting the video URL. Please try again');
            return redirect('/admin/dashboard/videos');
        }
    }
}
