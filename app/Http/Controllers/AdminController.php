<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\News;
use App\Image;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $userCount = User::all()->count();
        $newsCount = News::all()->count();
        $imageCount = Image::all()->count();
        return view('admin.index', compact('userCount', 'newsCount', 'imageCount'));
    }
}
