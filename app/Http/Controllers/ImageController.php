<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;

class ImageController extends Controller
{
    public function index(Image $image)
    {
        $image = $image->all()->where('published', '=', 1);
        return view('front.images.index', compact('images'));
    }
}
