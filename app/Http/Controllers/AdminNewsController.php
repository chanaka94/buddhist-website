<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
// use App\Repositories\Newss;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use DB;

class AdminNewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(News $news)
    {
        $allNews = $news->all();

        // $posts = (new \App\Repositories\Posts)->all();

        // Letting the query scope to handle the filter
        // $posts = Post::latest()
        //     ->filter(request(['month', 'year']))
        //     ->get();

    	return view('admin.manage-news.index', compact('allNews'));
    	// return view('admin.manage-news.index', compact('news'));
    }

    public function show($news)    // Using route model binding
    {
        $news = News::find($news);
    	return view('admin.manage-news.show', compact('news'));
    }

    public function create()
    {
    	return view('admin.manage-news.create');
    }

    public function store(Request $request)
    {
        // $this->validate(request(), [
        //     'title' => 'bail|required|max:100|min:3',
        //     'body' => 'bail|required|max:5000|min:10',
        //     'image' => 'bail|required',
        //     'url' => 'nullable|max:1000|min:5',
        //     'status' => 'bail|required|in:published,unpublished'
        // ]);

        $imagePath = Storage::disk('public')->put("news_images", request('image'));

        DB::beginTransaction();
        try {
            $news = News::create([
                'title' => request('title'),
                'body' => request('body'),
                'image' => $imagePath,
                'video' => request('url'),
                'published' => request('status')
            ]);
            DB::commit();
            session()->flash('success', 'Your news has now been added');
            // And then redirect to the dashboard
            return redirect('/admin/dashboard/news');
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            session()->flash('failed', 'Something went wrong when adding the news. Please try again.');
            return redirect('/admin/dashboard/news');
        }
    }

    public function edit($news)
    {
        $news = News::find($news);
    	return view('admin.manage-news.update', compact('news'));
    }

    public function update(Request $request, $id)
    {
        // $this->validate(request(), [
        //     'title' => 'bail|required|max:100|min:3',
        //     'body' => 'bail|required|max:5000|min:10',
        //     'image' => 'bail|required',
        //     'url' => 'nullable|max:1000|min:5',
        //     'status' => 'bail|required|in:published,unpublished'
        // ]);

        // Deleting the old image
        // $deletingFile = News::find($id);
        // Storage::delete('/public/storage/' . $deletingFile->image);
        // $imagePath = Storage::disk('public')->put("news_images", request('image'));

        DB::beginTransaction();
        try {
            DB::table('news')->where('id', '=', $id)->update([
                'title' => request('title'),
                'body' => request('body'),
                // 'image' => $imagePath,
                'video' => request('url'),
                'published' => request('status')
            ]);
            DB::commit();
            session()->flash('success', 'Your news has now been successfully updated.');
            return redirect('/admin/dashboard/news');
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            session()->flash('failed', 'Something went wrong when updating the news. Please try again.');
            return redirect('/admin/dashboard/news');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('news')->where('id', '=', $id)->delete();
            DB::commit();
            session()->flash('success', 'Successfully deleted the news');
            return redirect('/admin/dashboard/news');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('failed', 'Something went wrong when deleting the news. Please try again');
            return redirect('/admin/dashboard/faq');
        }
    }

}
