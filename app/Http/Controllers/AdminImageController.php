<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use DB;

class AdminImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(Image $image)
    {
        $allImages = $image->all();

        // $posts = (new \App\Repositories\Posts)->all();

        // Letting the query scope to handle the filter
        // $posts = Post::latest()
        //     ->filter(request(['month', 'year']))
        //     ->get();

    	return view('admin.manage-images.index', compact('allImages'));
    	// return view('admin.manage-images.index');
    }

    public function show($image)
    {
        $image = Image::find($image);
    	return view('admin.manage-images.show', compact('image'));
    }

    public function create()
    {
    	return view('admin.manage-images.create');
    }

    public function store(Request $request)
    {
        // $this->validate(request(), [
        //     'title' => 'bail|required|max:100|min:3',
        //     'body' => 'bail|required|max:5000|min:10',
        //     'image' => 'bail|required',
        //     'url' => 'nullable|max:1000|min:5',
        //     'status' => 'bail|required|in:published,unpublished'
        // ]);

        $imagePath = Storage::disk('public')->put("gallery/images", request('image'));

        DB::beginTransaction();
        try {
            $image = Image::create([
                'title' => request('title'),
                'path' => $imagePath,
                'published' => request('status')
            ]);
            DB::commit();
            session()->flash('success', 'Your image has now been added');
            // And then redirect to the dashboard
            return redirect('/admin/dashboard/images');
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback();
            session()->flash('failed', 'Something went wrong when adding the image. Please try again.');
            return redirect('/admin/dashboard/images');
        }
    }

    public function edit($image)
    {
        $image = Image::find($image);
    	return view('admin.manage-images.update', compact('image'));
    }

    public function update(Request $request, $id)
    {
        // $this->validate(request(), [
        //     'title' => 'bail|required|max:100|min:3',
        //     'body' => 'bail|required|max:5000|min:10',
        //     'image' => 'bail|required',
        //     'url' => 'nullable|max:1000|min:5',
        //     'status' => 'bail|required|in:published,unpublished'
        // ]);

        // Deleting the old image
        // $deletingFile = News::find($id);
        // Storage::delete('/public/storage/' . $deletingFile->image);
        // $imagePath = Storage::disk('public')->put("news_images", request('image'));

        DB::beginTransaction();
        try {
            DB::table('images')->where('id', '=', $id)->update([
                'title' => request('title'),
                'published' => request('status')
            ]);
            DB::commit();
            session()->flash('success', 'Your image has now been successfully updated.');
            return redirect('/admin/dashboard/images');
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback();
            session()->flash('failed', 'Something went wrong when updating the image. Please try again.');
            return redirect('/admin/dashboard/images');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('images')->where('id', '=', $id)->delete();

            // Image deletion to be handled
            // $image = Image::find($id);
            // \File::delete( public_path('/' . $image->path));
            
            DB::commit();
            session()->flash('success', 'Successfully deleted the image');
            return redirect('/admin/dashboard/images');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('failed', 'Something went wrong when deleting the image. Please try again');
            return redirect('/admin/dashboard/images');
        }
    }
}
