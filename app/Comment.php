<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function news()
	{
		return $this->belongsTo(News::class);
	}

	public function user()	// $comment->user->name; // This is only available because we have user()
	{
		return $this->belongsTo(User::class);
	}
}
