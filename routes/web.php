<?php

// Route::get('/', function () {
//     return view('welcome');
// });
//language
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});


// Route::get('/user', function() {
//     $user = App\User::forceCreate([
//         'username' => 'admin',
//         'email' => 'admin@example.com',
//         'password' => bcrypt('badmin'),
//         'role' => 'admin'
//     ]);
// });


/*
|--------------------------------------------------------------------------
| Admin Specific Routes
|--------------------------------------------------------------------------
|
| Following represents the admin routes
|
*/
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/admin/logout', 'SessionsController@destroy');

// Route::get('/admin', 'AdminController@index')->name('dashboard');
Route::get('/admin/dashboard', 'AdminController@index');

// Routes for the news section 
Route::get('/admin/dashboard/news', 'AdminNewsController@index');
Route::get('/admin/dashboard/news/create', 'AdminNewsController@create');
Route::post('/admin/dashboard/news', 'AdminNewsController@store');
Route::get('/admin/dashboard/news/show/{news}', 'AdminNewsController@show');
Route::get('/admin/dashboard/news/edit/{news}', 'AdminNewsController@edit');
Route::post('/admin/dashboard/news/update/{news}', 'AdminNewsController@update');
Route::get('/admin/dashboard/news/delete/{news}', 'AdminNewsController@destroy'); 

Route::get('/admin/dashboard/news/{tag}', 'TagsController@index');
Route::post('/admin/dashboard/news/{new}/comments', 'CommentsController@store');

// Routes for the images section 
Route::get('/admin/dashboard/images', 'AdminImageController@index');
Route::get('/admin/dashboard/images/create', 'AdminImageController@create');
Route::post('/admin/dashboard/images', 'AdminImageController@store');
Route::get('/admin/dashboard/images/show/{image}', 'AdminImageController@show');
Route::get('/admin/dashboard/images/edit/{image}', 'AdminImageController@edit');
Route::post('/admin/dashboard/images/update/{image}', 'AdminImageController@update');
Route::get('/admin/dashboard/images/delete/{image}', 'AdminImageController@destroy');

// Routes for the videos section 
Route::get('/admin/dashboard/videos', 'AdminVideoController@index');
Route::get('/admin/dashboard/videos/create', 'AdminVideoController@create');
Route::post('/admin/dashboard/videos', 'AdminVideoController@store');
Route::get('/admin/dashboard/videos/show/{video}', 'AdminVideoController@show');
Route::get('/admin/dashboard/videos/edit/{video}', 'AdminVideoController@edit');
Route::post('/admin/dashboard/videos/update/{video}', 'AdminVideoController@update');
Route::get('/admin/dashboard/videos/delete/{video}', 'AdminVideoController@destroy');





/*
|--------------------------------------------------------------------------
| Front Site Specific Routes
|--------------------------------------------------------------------------
|
| Following represents the front site routes
|
*/
Route::get('/', 'HomeController@index');
Route::get('/donation', 'HomeController@donation');
Route::get('/about-us', 'HomeController@aboutUs');
Route::get('/visit-plan', 'HomeController@visitPlan');
Route::get('/events-calendar', 'HomeController@eventsCalendar');
// Route::get('/contact-us', 'HomeController@contactUs');



Route::get('/news', 'NewsController@index');
Route::get('/news/{id}', 'NewsController@view');

Route::get('/gallery', 'GalleryController@index');
// Route::get('/gallery/{id}', 'GalleryController@view');

Route::get('/contact-us', 'ContactUsController@index');

Route::get('/radio-programs', 'RadioProgramsController@index');
