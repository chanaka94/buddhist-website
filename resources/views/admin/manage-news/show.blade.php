@extends('admin-layouts.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="/admin/plugins/light-gallery/css/lightgallery.min.css" rel="stylesheet">
@endsection

@section('content')
<?php $sidebar = 'manage_news'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-orange">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/bills">News</a></li>
                    <li class="active">View</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <h3 class="card-inside-title">News title</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $news->title }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                                               
                        <h3 class="card-inside-title">News image</h3>
                        <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <a href="/storage/{{ $news->image }}" data-sub-html="{{ $news->title }}">
                                    <img class="img-responsive thumbnail" src="/storage/{{ $news->image }}">
                                </a>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Date of the creation</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $news->created_at->diffForHumans() }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (isset($news->video))
                        <h3 class="card-inside-title">News video</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $news->video }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                
                        <h3 class="card-inside-title">News body</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="10" class="form-control no-resize" disabled>{!! $news->body !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Status</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        @if ($news->published == 1)
                                        <input type="text" class="form-control" value="Published" disabled />
                                        @endif
                                        @if ($news->published == 0)
                                        <input type="text" class="form-control" value="Unpublished" disabled />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <small>Uploaded on {{ $news->created_at->toFormattedDateString() }}</small>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Light Gallery Plugin Js -->
    <script src="/admin/plugins/light-gallery/js/lightgallery-all.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/pages/medias/image-gallery.js"></script>
    <script src="/admin/js/admin.js"></script>
@endsection
