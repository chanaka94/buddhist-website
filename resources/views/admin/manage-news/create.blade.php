@extends('admin-layouts.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />
@endsection

@section('content')
<?php $sidebar = 'manage_news'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-orange">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/news">News</a></li>
                    <li class="active">Create</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" data-reply-form action="/admin/dashboard/news" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="bill-id">News title <span style="color:red">*</span></label>
                                    <input type="text" id="title" class="form-control" name="title" value="{{ old('title') }}" required>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="ckeditor">News body <span style="color:red">*</span></label><br><br>
                                    <textarea id="ckeditor" class="form-control no-resize" minlength="5" name="body" required>{{ old('body') }}</textarea>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label for="image">Upload an image <span style="color:red">*</span></label>
                                <input type="file" name="image" accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);" style="padding: 8px 0px 8px 0px" required>
                                <img id="blah" src="https://via.placeholder.com/150" alt="your image" style="max-width:180px;" />
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="url">Enter an URL</label>
                                    <input type="url" id="url" class="form-control" name="url" value="{{ old('url') }}">
                                </div>
                                <div class="help-info">Starts with http://, https://, etc</div>
                            </div>
                            <div class="form-group form-float">
                                <label for="status">Status <span style="color:red">*</span></label>
                                <div class="demo-radio-button">
                                    <input name="status" type="radio" id="published" value="1" class="with-gap radio-col-indigo" />
                                    <label for="published">PUBLISH</label>
                                    <input name="status" type="radio" id="unpublished" value="0" class="with-gap radio-col-indigo" checked />
                                    <label for="unpublished">UNPUBLISH</label>
                                </div>
                            </div>
                            <br>
                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>

                        @include('errors')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Ckeditor -->
    <script src="/admin/plugins/ckeditor/ckeditor.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/admin.js"></script>
    <script src="/admin/js/pages/forms/form-validation.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
    <script src="/admin/js/pages/forms/basic-form-elements.js"></script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                    .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
