@extends('admin-layouts.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="/admin/plugins/light-gallery/css/lightgallery.min.css" rel="stylesheet">
@endsection

@section('content')
<?php $sidebar = 'manage_news'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-orange">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/news">News</a></li>
                    <li class="active">Update</li>
                </ol>

                <form id="form_advanced_validation" action="/admin/dashboard/news/update/{{ $news->id }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="body">
                            <h3 class="card-inside-title">News title <span style="color:red">*</span></h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line disabled">
                                            <input type="text" name="title" id="title" class="form-control" value="{{ $news->title }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="card-inside-title">News body <span style="color:red">*</span></h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            {{-- <textarea rows="10" class="form-control no-resize">{!! $news->body !!}</textarea> --}}
                                            <textarea id="ckeditor" class="form-control no-resize" minlength="5" name="body" required>{{ $news->body }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                                
                            <h3 class="card-inside-title">Current image</h3>
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <a href="/storage/{{ $news->image }}" data-sub-html="{{ $news->title }}">
                                        <img class="img-responsive thumbnail" src="/storage/{{ $news->image }}">
                                    </a>
                                    {{-- <h3 class="card-inside-title">New image <span style="color:red">*</span></h3>
                                    <input type="file" name="image" id="image" accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);" style="padding: 8px 0px 8px 0px">
                                    <img id="blah" src="https://via.placeholder.com/150" alt="Your image" style="max-width:180px;"> --}}
                                </div>
                            </div>

                            @if (isset($news->video))
                            <h3 class="card-inside-title">News video</h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line disabled">
                                            <input type="url" id="url" class="form-control" name="url" value="{{ $news->video }}">
                                            <div class="help-info">Starts with http://, https://, etc</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                    
                            <h3 class="card-inside-title">Status <span style="color:red">*</span></h3>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="demo-radio-button">
                                        <input name="status" type="radio" id="published" value="1" class="with-gap radio-col-indigo" @if ($news->published == 1) checked @endif>
                                        <label for="published">PUBLISH</label>
                                        <input name="status" type="radio" id="unpublished" value="0" class="with-gap radio-col-indigo" @if ($news->published == 0) checked @endif>
                                        <label for="unpublished">UNPUBLISH</label>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </div>
                    </div>
                </form>    
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Light Gallery Plugin Js -->
    <script src="/admin/plugins/light-gallery/js/lightgallery-all.min.js"></script>

    <!-- Ckeditor -->
    <script src="/admin/plugins/ckeditor/ckeditor.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/pages/medias/image-gallery.js"></script>
    <script src="/admin/js/admin.js"></script>
    <script src="/admin/js/pages/forms/form-validation.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
    <script src="/admin/js/pages/forms/basic-form-elements.js"></script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                    .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
