@extends('admin-layouts.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="/admin/plugins/light-gallery/css/lightgallery.min.css" rel="stylesheet">

    <!-- iFrame specific css -->
    <link href="/admin/css/iframe.css" rel="stylesheet">
@endsection

@section('content')
<?php $sidebar = 'manage_videos'; ?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-orange">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/videos">Videos</a></li>
                    <li class="active">View</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <h3 class="card-inside-title">Video title</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $video->title }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Video</h3>
                        <div class="iframe-container">
                            <iframe width="458" height="315"
                                src="{{ $video->url }}">
                            </iframe>
                        </div>

                        <h3 class="card-inside-title">Date of the creation</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $video->created_at->toFormattedDateString() }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Status</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        @if ($video->published == 1)
                                        <input type="text" class="form-control" value="Published" disabled />
                                        @endif
                                        @if ($video->published == 0)
                                        <input type="text" class="form-control" value="Unpublished" disabled />
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <small>Uploaded on {{ $video->created_at->diffForHumans() }}</small>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Light Gallery Plugin Js -->
    <script src="/admin/plugins/light-gallery/js/lightgallery-all.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/pages/medias/image-gallery.js"></script>
    <script src="/admin/js/admin.js"></script>
@endsection
