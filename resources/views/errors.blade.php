@if ($errors->any())
	<div class="form-group">
		<div class="alert alert-danger" style="margin-left:4px; margin-right:4px">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
@endif
