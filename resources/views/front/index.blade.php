@extends('front.master')

@section('content')

<!-- Start Hero Slider -->
<div class="hero-slider flexslider clearfix" data-autoplay="yes" data-pagination="yes" data-arrows="yes" data-style="fade" data-pause="yes">
    <ul class="slides">
        {{-- <li class=" parallax" style="background-image:url(http://placehold.it/1280x635&amp;text=IMAGE+PLACEHOLDER);"></li> --}}
        <li class="parallax" style="background-image:url(/images/meditating-monk.jpg);"></li>
        <li class="parallax" style="background-image:url(/images/theravada-buddhism.jpg);"></li>
        <li class="parallax" style="background-image:url(/images/meditation-by-the-river.jpg);"></li>
    </ul>
</div>
<!-- End Hero Slider --> 

<!-- Start Notice Bar -->
<div class="notice-bar latest-sermon">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-4 hidden-xs">
        	<h3><i class="fa fa-microphone"></i>Radio</h3>
        </div>
        <div class="col-md-7 col-sm-8 col-xs-12">
        	<audio class="audio-player" src="audio/Miaow-02-Hidden.mp3" type="audio/mp3" controls></audio>
        </div>
        <div class="col-md-2 hidden-sm hidden-xs">
        	<a href="sermons.html" class="btn btn-block btn-primary">All Programe</a>
        </div>
      </div>
    </div>
  </div>
  <!-- End Notice Bar --> 

<!-- Start Content -->
<div class="main" role="main">
    <div id="content" class="content full">
        <div class="container">
        <div class="row"> 
            <!-- Start Featured Blocks -->
            <div class="featured-blocks clearfix">
            <div class="col-md-4 col-sm-4 featured-block"> <a href="our-staff.html" class="img-thumbnail"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt="staff"> <strong>Our Team</strong> <span class="more">read more</span> </a> </div>
            <div class="col-md-4 col-sm-4 featured-block"> <a href="about.html" class="img-thumbnail"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt="staff"> <strong>About Us</strong> <span class="more">read more</span> </a> </div>
            <div class="col-md-4 col-sm-4 featured-block"> <a href="sermons.html" class="img-thumbnail"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt="staff"> <strong>Our News</strong> <span class="more">read more</span> </a> </div>
            </div>
            <!-- End Featured Blocks --> 
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-6"> 
            <!-- Events Listing -->
            <div class="listing events-listing">
                <header class="listing-header">
                <h3>More Coming Events</h3>
                </header>
                <section class="listing-cont">
                <ul>
                    <li class="item event-item">
                    <div class="event-date"> <span class="date">06</span> <span class="month">Aug</span> </div>
                    <div class="event-detail">
                        <h4><a href="single-event.html">Monday Prayer</a></h4>
                        <span class="event-dayntime meta-data">Monday | 07:00 AM</span> </div>
                    <div class="to-event-url">
                        <div><a href="single-event.html" class="btn btn-default btn-sm">Details</a></div>
                    </div>
                    </li>
                    <li class="item event-item">
                    <div class="event-date"> <span class="date">28</span> <span class="month">Aug</span> </div>
                    <div class="event-detail">
                        <h4><a href="single-event.html">Staff members meet</a></h4>
                        <span class="event-dayntime meta-data">Monday | 01:00 PM</span> </div>
                    <div class="to-event-url">
                        <div><a href="single-event.html" class="btn btn-default btn-sm">Details</a></div>
                    </div>
                    </li>
                    <li class="item event-item">
                    <div class="event-date"> <span class="date">25</span> <span class="month">Sep</span> </div>
                    <div class="event-detail">
                        <h4><a href="single-event.html">Evening Prayer</a></h4>
                        <span class="event-dayntime meta-data">Friday | 06:00 PM</span> </div>
                    <div class="to-event-url">
                        <div><a href="single-event.html" class="btn btn-default btn-sm">Details</a></div>
                    </div>
                    </li>
                </ul>
                </section>
            </div>
            <div class="spacer-30"></div>
            <!-- Latest News -->
            <div class="listing post-listing">
                <header class="listing-header">
                <h3>Latest News</h3>
                </header>
                <section class="listing-cont">
                <ul>
                    @foreach ($latestNews as $item)
                        
                    <li class="item post">
                        <div class="row">
                            <div class="col-md-4"> <a href="/news/{{ $item->id }}" class="media-box"> <img src="/storage/{{ $item->image }}" alt="" class="img-thumbnail"> </a></div>
                            <div class="col-md-8">
                                <div class="post-title">
                                    <h2><a href="blog-post.html">{{ $item->title }}</a></h2>
                                    <span class="meta-data"><i class="fa fa-calendar"></i> {{ $item->created_at->diffForHumans() }}</span>
                                </div>
                                <p>{!! $item->body !!}</p>
                            </div>
                        </div>
                    </li>
                    @endforeach

                    {{-- <li class="item post">
                    <div class="row">
                        <div class="col-md-4"> <a href="blog-post.html" class="media-box"> <img src="http://placehold.it/800x600&amp;text=IMAGE+PLACEHOLDER" alt="" class="img-thumbnail"> </a></div>
                        <div class="col-md-8">
                        <div class="post-title">
                            <h2><a href="blog-post.html">Voluptatum deleniti atque corrupti</a></h2>
                            <span class="meta-data"><i class="fa fa-calendar"></i> on 17th Dec, 2013</span></div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis fermentum sem, ac viverra ante luctus vel. Donec vel mauris quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus.</p>
                        </div>
                    </div>
                    </li> --}}

                </ul>
                </section>
            </div>
            </div>
            <!-- Start Sidebar -->
            <div class="col-md-4 col-sm-6"> 
            <!-- Latest Sermons -->
            <div class="listing sermons-listing">
                <header class="listing-header">
                <h3>Recent Sermons</h3>
                </header>
                <section class="listing-cont">
                <ul>
                    <li class="item sermon featured-sermon"> <span class="date">Feb 14, 2014</span>
                    <h4><a href="single-sermon.html">How To Recover The Cutting Edge</a></h4>
                    <div class="featured-sermon-video">
                        <iframe width="200" height="150" src="http://player.vimeo.com/video/19564018?title=0&amp;byline=0&amp;color=007F7B"></iframe>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis egestas rhoncus. Donec facilisis consectetur adipiscing elit. Nulla convallis egestas rhoncus</p>
                    <div class="sermon-actions"> <a href="#" data-placement="top" data-toggle="tooltip" data-original-title="Video"><i class="fa fa-video-camera"></i></a> <a href="#" data-placement="top" data-toggle="tooltip" data-original-title="Audio"><i class="fa fa-headphones"></i></a> <a href="#" data-placement="top" data-toggle="tooltip" data-original-title="Read online"><i class="fa fa-file-text-o"></i></a> <a href="#" data-placement="top" data-toggle="tooltip" data-original-title="Download PDF"><i class="fa fa-book"></i></a> </div>
                    </li>
                    <li class="item sermon">
                    <h2 class="sermon-title"><a href="single-sermon.html">Voluptatum deleniti atque corrupti</a></h2>
                    <span class="meta-data"><i class="fa fa-calendar"></i> on 17th Dec, 2013</span> </li>
                    <li class="item sermon">
                    <h2 class="sermon-title"><a href="single-sermon.html">Voluptatum deleniti atque corrupti</a></h2>
                    <span class="meta-data"><i class="fa fa-calendar"></i> on 17th Dec, 2013</span> </li>
                    <li class="item sermon">
                    <h2 class="sermon-title"><a href="single-sermon.html">Voluptatum deleniti atque corrupti</a></h2>
                    <span class="meta-data"><i class="fa fa-calendar"></i> on 17th Dec, 2013</span> </li>
                </ul>
                </section>
            </div>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- End Content -->
<div class = "container">
    <div class = "row">
    </div>

</div>



<!-- Start Featured Gallery -->
<div class="featured-gallery">
    <div class="container">
        <div class="row">
        <div class="col-md-3 col-sm-3">
            <h4>Updates from our gallery</h4>
            <a href="/gallery" class="btn btn-default btn-lg">More Galleries</a> </div>

        @foreach ($latestImages as $latestImage)  
        <div class="col-md-3 col-sm-3 post format-image"> <a href="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" class="media-box" data-rel="prettyPhoto[Gallery]"> <img src="/storage/{{ $latestImage->path }}" alt=""> </a> </div>
        @endforeach

        @foreach ($latestVideos as $latestVideo)
        {{-- <div class="col-md-3 col-sm-3 post format-video"> <a href="{{ $latestVideo->url }}" class="media-box" data-rel="prettyPhoto[Gallery]"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </a> </div> --}}
        <div class="col-md-3 col-sm-3 post format-video"> <a href="http://youtu.be/NEFfnbQlGo8" class="media-box" data-rel="prettyPhoto[Gallery]"> <img src="http://placehold.it/600x400&amp;text=IMAGE+PLACEHOLDER" alt=""> </a> </div>
        @endforeach
        </div>
    </div>
</div>
<!-- End Featured Gallery -->

@endsection
