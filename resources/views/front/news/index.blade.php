@extends('front.master')

@section('content')
    <!-- Start Nav Backed Header -->
  <div class="nav-backed-header parallax" style="background-image:url(/images/banner.jpg);">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">News</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!-- End Nav Backed Header --> 
  <!-- Start Page Header -->
  <div class="page-header">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>News</h1>
        </div>
      </div>
    </div>
  </div>
  <!-- End Page Header --> 
  <!-- Start Content -->
  <div class="main" role="main">
    <div id="content" class="content full">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul class="grid-holder col-3 events-grid">

              @foreach ($news as $item)

              <li class="grid-item post format-standard">
                <div class="grid-item-inner"> <a href="/storage/{{ $item->image }}" data-rel="prettyPhoto" class="media-box"> <img src="/storage/{{ $item->image }}" alt=""> </a>
                  <div class="grid-content">
                    <h3><a href="/news/{{ $item->id }}" style="font-size:18px">{{ $item->title }}</a></h3>
                    <span class="meta-data"><span><i class="fa fa-calendar"></i> {{ $item->created_at }}</span></span>
                    <p>{{ htmlspecialchars(substr(strip_tags($item->body), 0, 300)) }}</p>
                    {{-- {{ $item->body }} --}}
                  </div>
                </div>
              </li>
              @endforeach
              
            </ul>
            
            <!-- Pagination -->
            <ul class="pager pull-right">
              <li><a href="#">&larr; Older</a></li>
              <li><a href="#">Newer &rarr;</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
