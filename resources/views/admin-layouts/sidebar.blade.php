<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li @if ($sidebar == 'dashboard') class="active" @endif>
                    <a href="/admin/dashboard">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li @if ($sidebar == 'manage_news') class="active" @endif>
                    <a href="/admin/dashboard/news">
                        <i class="material-icons">forum</i>
                        <span>Manage News</span>
                    </a>
                </li>
                <li @if ($sidebar == 'manage_images') class="active" @endif>
                    <a href="/admin/dashboard/images">
                        <i class="material-icons">image</i>
                        <span>Manage Gallery Images</span>
                    </a>
                </li>
                <li @if ($sidebar == 'manage_videos') class="active" @endif>
                    <a href="/admin/dashboard/videos">
                        <i class="material-icons">ondemand_video</i>
                        <span>Manage Gallery Videos</span>
                    </a>
                </li>
                <li @if ($sidebar == 'manage_gallery') class="active" @endif>
                    <a href="/admin/dashboard/gallery">
                        <i class="material-icons">photo_library</i>
                        <span>Manage Gallery Items</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="material-icons">radio</i>
                        <span>Manage Radio Programs</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="material-icons">settings</i>
                        <span>Account Settings</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2019 <a href="javascript:void(0);">All Rights Reserved</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
