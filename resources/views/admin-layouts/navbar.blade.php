<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="/admin/dashboard">ADMIN DASHBOARD</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">notifications</i>
                        <span class="label-count"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">NOTIFICATIONS</li>
                        <li class="body">
                            <ul class="menu">
                                
                                <li>
                                    <a href="#">
                                        <div class="icon-circle bg-cyan">
                                            <i class="material-icons">forum</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Comming soon...</h4>
                                            <p>
                                                <i class="material-icons">access_time</i>
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all comments</a> 
                        </li>
                    </ul>
                </li>
                <!-- #END# Notifications -->
                
                <li class="drop-down">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="/admin/logout"><i class="material-icons">exit_to_app</i>Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
